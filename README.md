# Simulating Rosbot and the World
## for checking out Mowito's Navigation Stack

Adapted from Husarion  https://github.com/husarion/rosbot_description

## Installation. ## 

Pre-requisite: install ROS 1 (kinetic or later) based on your ubuntu version.

Prepare the repository:
```
cd ~
mkdir -p mowito_ws/src
cd ~/mowito_ws/src
catkin_init_workspace
cd ~/mowito_ws
catkin_make
```

Above commands should execute without any warnings or errors.


Clone this repository to your workspace:

```
cd ~/mowito_ws/src
git clone https://gitlab.com/mowito_release/mowito_sim.git
```

Install depencencies:

```
cd ~/mowito_ws
rosdep install --from-paths src --ignore-src -r -y
```

Build the workspace:

```
cd ~/mowito_ws
catkin_make
```

From this moment you can use rosbot simulations. Please remember that each time, when you open new terminal window, you will need to load system variables:

`source ~/mowito_ws/devel/setup.sh`


## How to use ##
Now, since the SIM is setup, you are ready to try out all the features of Mowito:

### A. Running Navigation with no Map / Navigation to create Map

1. create a map using either of these three methods:<br><br>
1.1. **manual navigation**<br>
`source ~/mowito_ws/devel/setup.bash`<br>
`roslaunch rosbot_description rosbot_rviz_mapping.launch`<br><br>
in another terminal, start the remote control<br>
`source ~/mowito_ws/devel/setup.bash`<br>
`rosrun teleop_twist_keyboard teleop_twist_keyboard`<br>
and use it move the robot around<br><br>

1.2. **Navigation, by giving goals through the rviz**<br>
`source ~/mowito_ws/devel/setup.bash`<br>
`roslaunch rosbot_description rosbot_rviz_SLAM_nav.launch`<br><br>
on rviz, give goals on the map, and the robot will move autnomously while creating the map<br><br>

1.3. **Autonomous goal selection ,throuh Exploration**<br>
`source ~/mowito_ws/devel/setup.bash`<br>
`roslaunch rosbot_description rosbot_rviz_SLAM_explore.launch`<br><br>
on rviz you can see the robot automatically moving and exploring the area<br><br>

2. Once you are done creating the map on rviz, save the map <br>
on a new terminal exeute the following <br>
`source ~/mowito_ws/devel/setup.bash`<br>
`cd && rosrun map_server map_saver -f mymap`<br>
the map (pgm and yaml) is saved  in the home directory with the name mymap.pgm and mymap.yaml<br><br>

### B. Running Navigation  with a pre-exitsting Map


1. Place the robot at the origin of map (the place where you started mapping)<br>
 
2. Now, for running the entire system with mowito’s controller run<br>  
`source ~/mowito_ws/devel/setup.bash`<br>
`roslaunch rosbot_description rosbot_rviz_mwpfl_nav.launch map_name:=mymap`<br>

3. in the rviz, click on the second top panel, click on the nav goal option, and click on the displayed map to give goal to the robot<br>

4. look at the output on the rviz, the path planned and the motion of the robot.<br>



## Tips ##

If you have any problems with laser scan it probably means that you don't have a dedicated graphic card (or lack appropriate drivers). If that's the case then you'll have to change couple of things in /rosbot_description/urdf/rosbot_gazebo file:

Find:   `<!-- If you cant't use your GPU comment RpLidar using GPU and uncomment RpLidar using CPU gazebo plugin. -->`
next coment RpLidar using GPU using `<!-- -->` from `<gazebo>` to `</gazebo>` like below:

 ```
 <!-- gazebo reference="rplidar">
   <sensor type="gpu_ray" name="head_rplidar_sensor">
     <pose>0 0 0 0 0 0</pose>
     <visualize>false</visualize>
     <update_rate>40</update_rate>
     <ray>
       <scan>
         <horizontal>
           <samples>720</samples>
           <resolution>1</resolution>
           <min_angle>-3.14159265</min_angle>
           <max_angle>3.14159265</max_angle>
         </horizontal>
       </scan>
       <range>
         <min>0.2</min>
         <max>30.0</max>
         <resolution>0.01</resolution>
       </range>
       <noise>
         <type>gaussian</type>
         <mean>0.0</mean>
         <stddev>0.01</stddev>
       </noise>
     </ray>
     <plugin name="gazebo_ros_head_rplidar_controller" filename="libgazebo_ros_gpu_laser.so">
       <topicName>/rosbot/laser/scan</topicName>
       <frameName>rplidar</frameName>
     </plugin>
   </sensor>
 </gazebo -->
```

Now uncomment RpLidar using CPU plugin removing `<!-- -->`.

If you want to make your laser scan visible just change:
```
<visualize>false</visualize>
```
to:
```
<visualize>true</visualize>
```
in the same plug in.
